# pyServerExample

run the server using 
env FLASK_APP=scriptServer.py flask run

Then send it a curl to hit your code.  

curl -X POST \
  http://127.0.0.1:5000/ \
  -H 'authorization: 3f3fa7ca-44b2-11e9-b210-d663bd873d93' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: bf76b0fd-e820-7563-5ea1-6cbc4e372ba0' \
  -d '{
	"some": "value"
}'

Note the Authorization header is fixed.  You prob want to change this and read this from a file or something. THE FILE SHOULD NOT BE CHECKED INTO SOURCE CONTROL.


