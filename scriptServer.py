from flask import Flask
from flask import request
from flask import Response
import json

app = Flask(__name__)

@app.route('/', methods = ['POST'])
def hello():

    ####AUTH section
    print('I got a request.  YAY')
    # some basic auth.  Dont use this random UUID in reality.  make a new one and read it fomr a file outside of source control.  
    # Or alternatively use some other type of auth mechanism.   but wither way check it here
    print(request.headers['Authorization'])
    if request.headers['Authorization'] != '3f3fa7ca-44b2-11e9-b210-d663bd873d93':  
        return "Piss OFF", 403
    
    print('It had a safe header. YAY')


    ####YOUR CODE section
    body = request.data
    #You code to run your script fun goes here.  check the body variable for stuff you need
    print('It had a hot body. YAY')
    print('You sent me: ', body)


    ####RESPONSE section. 
    #now for some handy response stuff where you can customise the response http status code, the returned data and the return type (which prob should be json)
    data = {
        'message'  : 'hello Mr sumo webhook.  I got your data.  now go back to sleep',
        'number' : 3
    }
    js = json.dumps(data)

    resp = Response(js, status=200, mimetype='application/json')
    return resp